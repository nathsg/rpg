import { createStore, combineReducers } from 'redux'

import playerReducer from '../features/player/reducer'
import groundReducer from '../features/ground/reducer'

const rootReducer = combineReducers({
    player: playerReducer,
    ground: groundReducer,
})

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)

export default store