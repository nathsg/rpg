import React from 'react'
import { connect } from 'react-redux'


import { SPRITE_SIZE } from '../../config/constants'

import './styles.css'

function getTileSprite(type) {
	switch(type) {
		case 0:
			return 'grass'
		case 5:
			return 'rock'
		case 6:
			return 'tree'
		default:
			return
	}
}

function GroundTile(props) {
	return (
		<div
			className={`tile ${getTileSprite(props.tile)}`}
			style={{
				height: SPRITE_SIZE,
				width: SPRITE_SIZE,
			}}
		/>
	)
}

function GroundRow(props) {
	return (
		<div
			className='row'
			style={{
				height: SPRITE_SIZE,
			}}
		>
			{
				props.tiles.map( tile => <GroundTile tile={tile} />)
			}
		</div>
	)
}

function Ground(props) {
	return(
		<div
			style={{
				position: 'relative',
				top: '0px',
				left: '0px',
				width: '1800px',
				height: '900px',
				border: '4px solid white',
			}}
		>
			{
				props.tiles.map( row => <GroundRow tiles={row} />)
			}
		</div>

	)
}

function mapStateToProps(state) {
	return {
		tiles: state.ground.tiles
	}
}


export default connect(mapStateToProps)(Ground)