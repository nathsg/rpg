import React from 'react'

import Ground from '../ground'
import Player from '../player'

import tiles from '../../data/grounds/1'
import store from '../../config/store'

function World(props) {
	store.dispatch({
		type: 'ADD_TILES',
		payload: {
			tiles,
		}
	})

	return(
		<div
			style={{
				position: 'relative',
				width: '1800px',
				height: '900px',
				margin: '20px auto',
			}}
		>
			<Ground />
			<Player />
		</div>
	)
}

export default World